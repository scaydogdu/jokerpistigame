﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {
    public GameObject panelStart,panelEnd;
    public GameObject[] winLoseText;
    public void StartGame()
    {
        panelStart.SetActive(false);
		GameManager.instance.OnStart(this);
    }

    public  void OnPause(bool openMenu)
    {
        
    }

    public  void OnLevelComplete(int winner)
    {
      //  base.OnLevelComplete(winner);
        panelEnd.SetActive(true);
        winLoseText[winner].SetActive(true);
    }
    public void ReloadLevel()
    {
     
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
