﻿using UnityEngine;
using System.Collections;

public class Card
{
    public readonly static int ACE = 1;      
    public readonly static int JACK = 11;    
    public readonly static int QUEEN = 12;   
    public readonly static int KING = 13;

    public CardSuit cardSuit;
    public int number;

    public Card(CardSuit suit, int number)
    {
        this.cardSuit = suit;
        this.number = number;
    }

    public string NumericName
    {
        get
        {
            switch (number)
            {
                case 1:
                    return "A";
                case 11:
                    return "J";
                case 12:
                    return "Q";
                case 13:
                    return "K";
                default:
                    return number.ToString();
            }
        }
    }


	//TODO: Generic scores reading from somewhere.
    public int GetScore()
    {
		int val = 0;
        switch (number)
        {
            case 1:
				val = 1;
				break;
			case 2:
				if(CardSuit.Spades == this.cardSuit)
				{
					val = 2;
				}
				break;
			case 10:
				if(CardSuit.Diamonds == this.cardSuit)
				{
					val = 3;
				}
				break;			
			case 11:
				val = 1;
				break;
        }

		return val;
    }
}

public enum CardSuit
{
    Clubs, Diamonds, Hearts, Spades
}
