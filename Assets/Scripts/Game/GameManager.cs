﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class GameManager : MonoBehaviour
{

	static GameManager _instance = null;
	private UIController uiManager;

    private List<Card> _playedCards = new List<Card>();
    private Card _lastPlayedCard = null;
    private int _currentScore;
	private int _playerOrder = 0;

 
	[SerializeField]
	private Button _buttonPileCards;
	[SerializeField]
	private DeckManager _deck;
	[SerializeField]
	private PlayerBase[] _players;
	[SerializeField]
	private Text _textTableScore;

	public static GameManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(GameManager)) as GameManager;

				if(!_instance)
				{
					var obj = new GameObject("GameManager");
					_instance = obj.AddComponent<GameManager>();
				}
			}
			return _instance;
		}
	}

    public void OnStart(UIController uiMng)
    {    
		this.uiManager = uiMng;
        InitTable();
    }


	void Update()
	{
		
		///if (Input.GetMouseButtonDown(1))
		//	Debug.Log("Pressed right click.");
	}

    void InitTable()
    {
        for (int i = 0; i < 4; i++)
        {
            Card card = _deck.GetRandomCard();
            _playedCards.Add(card);
            CurrentScore += card.GetScore();
            _lastPlayedCard = card;

			//TODO: use StringBuilder
			_buttonPileCards.GetComponent<Image>().sprite = Resources.Load<Sprite>("card"+card.cardSuit.ToString()+card.NumericName);
        }

        DealCards();

    }

    private void DealCards()
    {
        if (_deck.NumberOfCards > 0)
        {
            Debug.Log("Dealing");
            for (int i = 0; i < _players.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Card card = _deck.GetRandomCard();
                    _players[i].AddCard(j, card);
                }
            }
            GiveTurnToCurrentPlayer();
        }
        else
        {
            // no cards end of game.
			CheckMostCardsCollected();

            Debug.Log("cards finished");
            int score = 0;
            int winner = 0;

            for (int i = 0; i < _players.Length; i++)
            {
                if (_players[i].Score > score)
                {
                    score = _players[i].Score;
                    winner = i;
                }
            }
			uiManager.OnLevelComplete(winner);
        }
    }

    public void PlayCard(Card card)
    {
        _playedCards.Add(card);
        CurrentScore += card.GetScore();
        // show them 

		//TODO: String Builder.
		_buttonPileCards.GetComponent<Image>().sprite = Resources.Load<Sprite>("card"+card.cardSuit.ToString()+card.NumericName);        
        // check card
        if (_lastPlayedCard == null)
        {
            Debug.Log("First card");
            _lastPlayedCard = card;
            CheckPlayersStatus();
            return;
        }
        // we have thrown card
        if (card.number == _lastPlayedCard.number)
        {
            if (_playedCards.Count == 2)
            {
                // Pisti take it 
                Debug.Log("Pisti");
                CurrentScore += 10;
                TakeCardsForCurrentPlayer();
                return;
            }
            // normal take it
            TakeCardsForCurrentPlayer();
            return;
        }
        else if (card.number == Card.JACK)
        {
            // take it all
            TakeCardsForCurrentPlayer();
            return;
        }
        // else continue
        _lastPlayedCard = card;
        // check status
        CheckPlayersStatus();
    }

    private void TakeCardsForCurrentPlayer()
    {
		
        Debug.Log("taking card "+ PlayerOrder);
        // reset UI 
		_buttonPileCards.GetComponent<Image>().sprite = null; //Resources.Load<Sprite>("cardBack_blue1");

		_players[PlayerOrder].cardsCollected += _playedCards.Count;
			
		_lastPlayedCard = null;
        _playedCards = new List<Card>();

			
		AddScoreToPlayer();
        // check status
        CheckPlayersStatus();
    }   

	private void CheckMostCardsCollected()
	{
		int mostCards = 0;
		int playerToGetBonus = 0;
		for (int i = 0; i < _players.Length; i++)
		{
			if(_players[i].cardsCollected > mostCards)
			{
				playerToGetBonus = i;
				mostCards = _players[i].cardsCollected;
			}
		}

		_players[playerToGetBonus].Score += 3;
	}

    private void CheckPlayersStatus()
    {
        // change player order
        PlayerOrder++;
        for (int i = 0; i < _players.Length; i++)
        {
            // if we have cards continue
            if (_players[i].CardCount > 0)
            {
                GiveTurnToCurrentPlayer();
                return;
            }
        }
        // else deal card
        DealCards();
    }

    private void GiveTurnToCurrentPlayer()
    {
        _players[PlayerOrder].IsMyTurn = true;
    }
    private void AddScoreToPlayer()
    {
        _players[PlayerOrder].Score += CurrentScore;

        CurrentScore = 0;
    }

	private int PlayerOrder
	{
		get { return _playerOrder; }
		set
		{
			_playerOrder = value;
			if (_playerOrder > _players.Length - 1)
				_playerOrder = 0;
		}
	}
	private int CurrentScore
	{
		get { return _currentScore; }
		set
		{
			_currentScore = value;
			_textTableScore.text = "Table Sum: " + _currentScore;
		}
	}
}
