﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class PlayerAI : PlayerBase
{
	[SerializeField]
	private Button[] _buttonCards;

	private int _remainingCards = 0;

    public override bool IsMyTurn
    {
        get
        {
            return _isMyTurn;
        }
        set
        {
            _isMyTurn = value;
            if (_isMyTurn)
            {
                int index = Random.Range(0, mDeck.Count);
                PlayCard(index);
            }
        }
    }

	public override void AddCard(int index, Card card)
	{
		base.AddCard(index, card);

		for (int i = 0; i < base.CardCount; i++)
		{
			_buttonCards[i].interactable = false;
			//TODO: use StringBuilder 
			_buttonCards[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("cardBack_blue1");

		}

		_remainingCards = 4;
	}

	private void SetMyCards()
	{


	}

    public override void PlayCard(int index)
    {
        // get random card
        int key = mDeck.ElementAt(index).Key;
        Card c = mDeck[key];
        mDeck.Remove(key);
		_buttonCards[--_remainingCards].GetComponent<Image>().sprite = null;

		Debug.Log("Player AI Played card " +c.cardSuit+ c.NumericName);
        mGame.PlayCard(c);
        IsMyTurn = false;
        
    }
}
