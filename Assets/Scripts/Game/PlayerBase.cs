﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public abstract class PlayerBase : MonoBehaviour
{
    public int Score 
    { 
        get { return _score; } 
        set 
        { 
            _score = value; 
			//TODO: String Builder
            _textScore.text = "Score " + _score; 
        }
    }
    public int CardCount { get { return mDeck.Count; } }
	public int cardsCollected = 0;
    protected Dictionary<int, Card> mDeck = new Dictionary<int, Card>();
    protected GameManager mGame;
    protected bool _isMyTurn;
    [SerializeField]
    protected Text _textScore;
    private int _score;
    
    public abstract bool IsMyTurn { get; set; }
    public abstract void PlayCard(int index);

    public virtual void Start()
    {
        mGame = FindObjectOfType<GameManager>();
    }

    public virtual void AddCard(int index,Card card)
    {
        mDeck.Add(index, card);
    }
		
}
